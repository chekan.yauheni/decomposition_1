# Task. Decomposition using methods (subroutines).

### Solve the tasks:

1. Write a method(s) for finding the greatest common divisor (GCD) and the 
smallest common multiple (SCM) of two natural numbers:
`SCM(a, b) = (a * b) / GCD(a, b)`

2. Write a method(s) for finding the greatest common divisor of four natural numbers.

3. Write a method(s) for finding the smallest common multiple of three natural numbers.

4. Write a method(s) for calculating the sum of factorials of all odd numbers from 1 to 9.

5. Create a program that finds the second-largest number in the array A[N] (print out the number that is smaller 
than the maximum element of the array, but larger than all other elements).

First three items are implemented in `CommonDivisorMultiple` class.
