package com.epam.rd;

public class SumFactorials {
    public static void main(String[] args) {
        int numRange = 9;
        int sumOddFactorials = sumOddFactorials(numRange);
        System.out.printf("Sum of factorials of odd numbers in range %s = %s\n", numRange, sumOddFactorials);
    }

    public static int findFactorial(int number) {
        int index = 1;
        int factorial = 1;
        while(index <= number) {
            factorial *= index;
            index++;
        } return factorial;
    }

    public static int sumOddFactorials(int number) {
        int sum = 0;
        for(int i = 1; i <= number; i += 2) {
            sum += findFactorial(i);
        } return sum;
    }
}
