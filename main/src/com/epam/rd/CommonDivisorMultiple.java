package com.epam.rd;

public class CommonDivisorMultiple {

    public static void main(String[] args) {
        int a = 24;
        int b = 16;
        int c = 5;
        int d = 3;

        int gcd = gcdForTwo(a, b);
        int lcm = lcmForTwo(a, b);
        int gcdFour = gcdForFour(a, b, c, d);
        int lcmThree = lcmForThree(a, b, c);

        System.out.printf("Greatest common divisor of %s and %s = %s\n", a, b, gcd);
        System.out.printf("Least common multiple of %s and %s = %s\n", a, b, lcm);
        System.out.printf("Greatest common divisor of %s, %s, %s and %s = %s\n", a, b, c, d, gcdFour);
        System.out.printf("Least common multiple of %s, %s and %s = %s\n", a, b, c, lcmThree);
    }

    public static int gcdForTwo(int firstNum, int secondNum) {
        /* Calculate the greatest common divisor for two natural numbers
         * using standard Euclidean algorithm. */
        while (secondNum != 0) {
            int temp = secondNum;
            secondNum = firstNum % temp;
            firstNum = temp;
        } return firstNum;
    }

    public static int lcmForTwo(int firstNum, int secondNum) {
        // Calculate the least common multiple for two natural numbers
        return (firstNum * secondNum) / gcdForTwo(firstNum, secondNum);
    }

    public static int gcdForFour(int firstNum, int secondNum, int thirdNum, int fourthNum) {
        // Calculate the greatest common divisor for four numbers
        int gcd = gcdForTwo(firstNum, secondNum);
        gcd = gcdForTwo(gcd, thirdNum);
        gcd = gcdForTwo(gcd, fourthNum);
        return gcd;
    }

    public static int lcmForThree(int firstNum, int secondNum, int thirdNum) {
        // Calculate the least common multiple for three numbers
        int lcm = lcmForTwo(firstNum, secondNum);
        lcm = lcmForTwo(lcm, thirdNum);
        return lcm;
    }
}
