package com.epam.rd;

import java.util.Arrays;
import java.util.Random;

public class SecondLargest {
    public static void main(String[] args) {
        int[] numArray = generateArray(25);

        int secondLargest = findSecondLargest(numArray);

        System.out.printf("Initial array: %s\n", Arrays.toString(numArray));
        System.out.printf("Second largest in the array: %s\n", secondLargest);
    }

    public static int[] generateArray(int arrayLength) {
        Random rnd = new Random();
        int[] newArray = new int[arrayLength];
        for(int i = 0; i < newArray.length; i++) {
            newArray[i] = rnd.nextInt(100);
        } return newArray;
    }

    public static int findSecondLargest(int[] arrayToSearch) {
        int maxElement = arrayToSearch[0];
        int secondLargest = maxElement - 1;
        for(int i = 1; i < arrayToSearch.length; i++) {
            int currentElement = arrayToSearch[i];
            if(currentElement > maxElement) {
                int temp = maxElement;
                maxElement = currentElement;
                secondLargest = temp;
            }
            if(currentElement > secondLargest && currentElement < maxElement) {
                secondLargest = currentElement;
            }
        } return secondLargest;
    }
}
